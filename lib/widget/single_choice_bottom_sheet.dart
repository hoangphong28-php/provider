import 'package:base_provider/common/extension/extension.dart';
import 'package:flutter/material.dart';

import '../di/locator.shorten.dart';
import 'base_item.dart';
import 'input/input.dart';
import 'loading.dart';


class SingleChoiceBottomSheet<T extends BaseItem> extends StatefulWidget {
  final String? label;
  final String? hint;
  final List<T>? items;
  final Future<List<T>> Function()? getInitialItems;
  final T? selectedItem;
  final ValueChanged<T> onChangeItem;
  final bool isDisable;
  final VoidCallback? onDisableAlert;
  final Color? fillColor;

  SingleChoiceBottomSheet(
      {super.key,
      this.label,
      this.hint = '-Lựa chọn-',
      this.items,
      this.getInitialItems,
      this.selectedItem,
      required this.onChangeItem,
      this.isDisable = false,
      this.onDisableAlert,
      fillColor}) : fillColor = fillColor ?? AppLocator.color.grey.shade400;

  @override
  State<SingleChoiceBottomSheet<T>> createState() =>
      _SingleChoiceBottomSheetState<T>();
}

class _SingleChoiceBottomSheetState<T extends BaseItem>
    extends State<SingleChoiceBottomSheet<T>> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        (widget.label != null)
            ? Text(
                widget.label!,
                style: AppLocator.theme.textThemeGrey.m,
              )
            : const SizedBox(),
        (widget.label != null) ? const SizedBox(height: 8) : const SizedBox(),
        GestureDetector(
          onTap: () {
            if (!widget.isDisable) {
              showModalBottomSheet(
                isScrollControlled: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(12),
                      topRight: Radius.circular(12)),
                ),
                context: context,
                builder: (context) {
                  return _SingleBottomSheetBody(
                      label: widget.label ?? '',
                      items: widget.items,
                      getInitialItems: widget.getInitialItems,
                      selectedItem: widget.selectedItem,
                      onChangeItem: widget.onChangeItem);
                },
              );
            } else {
              if (widget.onDisableAlert != null) {
                widget.onDisableAlert!.call();
              }
            }
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 14),
            decoration: BoxDecoration(
                color: widget.fillColor,
                borderRadius: BorderRadius.circular(8)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(widget.selectedItem?.displayName ?? widget.hint!,
                      overflow: TextOverflow.ellipsis,
                      style: (widget.selectedItem != null)
                          ? AppLocator.theme.textThemeBlack.m
                          : AppLocator.theme.textThemeLGrey.m),
                ),
                Icon(
                  Icons.keyboard_arrow_down_rounded,
                  size: 16,
                  color: AppLocator.color.black,
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

class _SingleBottomSheetBody<T extends BaseItem> extends StatefulWidget {
  final String label;
  final List<T>? items;
  final Future<List<T>> Function()? getInitialItems;
  final T? selectedItem;
  final ValueChanged<T> onChangeItem;

  const _SingleBottomSheetBody(
      {super.key,
      required this.label,
      this.items,
      this.getInitialItems,
      this.selectedItem,
      required this.onChangeItem});

  @override
  State<_SingleBottomSheetBody<T>> createState() =>
      _SingleBottomSheetBodyState<T>();
}

class _SingleBottomSheetBodyState<T extends BaseItem>
    extends State<_SingleBottomSheetBody<T>> {
  List<T> displayItems = <T>[];
  List<T>? apiItems;
  T? tempSelectedItem;
  bool processing = false;
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    if (widget.getInitialItems != null) {
      fetchItem();
    } else {
      generateDisplayItems();
    }
    super.initState();
  }

  void fetchItem() async {
    setState(() {
      processing = true;
    });
    try {
      apiItems = await widget.getInitialItems!.call();
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      setState(() {
        generateDisplayItems();
        processing = false;
      });
    }
  }

  void searchData() {
    List<T> searchItems = [];
    generateDisplayItems();
    searchItems.addAll(displayItems);
    if (searchController.text.isNotEmpty) {
      displayItems.clear();
      for (int i = 0; i < searchItems.length; i++) {
        if (searchItems[i]
            .displayName
            .toLowerCase()
            .trim()
            .contains(searchController.text.toLowerCase().trim())) {
          displayItems.add(searchItems[i]);
        }
      }
    } else {
      generateDisplayItems();
    }
  }

  bool getSelectedBottomSheetModel(T model) {
    if (model.uniqueData == tempSelectedItem?.uniqueData) {
      return true;
    } else {
      return false;
    }
  }

  void generateDisplayItems() {
    displayItems.clear();
    if (apiItems != null) {
      displayItems.addAll(apiItems!);
    } else {
      displayItems.addAll(widget.items!);
    }
    if (displayItems.isEmpty) {
      tempSelectedItem = null;
    } else {
      tempSelectedItem = widget.selectedItem;
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12), topRight: Radius.circular(12)),
      ),
      height: height - 120,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox(
                height: 44,
                width: 44,
              ),
              Text(widget.label,
                  style: AppLocator.theme.textThemeBlack.m.regular),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: SizedBox(
                  height: 44,
                  width: 44,
                  child: Icon(Icons.close,
                      size: 24, color: AppLocator.color.black),
                ),
              ),
            ],
          ),
          Divider(height: 1, color: AppLocator.color.grey.shade400),
          Expanded(
            child: StackLoading.medium(
                processing: processing,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Input.text(
                                    hintText: 'Tìm kiếm',
                                    controller: searchController,
                                    onChanged: (res) {
                                        setState(() {
                                          searchData();
                                        });
                                    
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  width: 12,
                                ),
                                GestureDetector(
                                    child: const SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: Icon(Icons.search),
                                )),
                              ],
                            ),
                            (displayItems.isNotEmpty)
                                ? Expanded(
                                    child: ListView.separated(
                                        padding: EdgeInsets.zero,
                                        shrinkWrap: true,
                                        // physics:
                                        //     const NeverScrollableScrollPhysics(),
                                        itemCount: displayItems.length,
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                            onTap: () {
                                              // select model
                                              setState(() {
                                                tempSelectedItem =
                                                    displayItems[index];
                                              });
                                              if (tempSelectedItem != null) {
                                                FocusScope.of(context)
                                                    .unfocus();
                                                widget.onChangeItem(
                                                    tempSelectedItem!);
                                                Navigator.pop(context);
                                              }
                                            },
                                            child: Container(
                                              color: Colors.transparent,
                                              // this is for easier tap detection
                                              child: Column(
                                                children: [
                                                  const SizedBox(height: 16),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Expanded(
                                                        child: Text(
                                                            displayItems[index]
                                                                .displayName,
                                                            style: AppLocator
                                                                .theme
                                                                .textThemeBlack
                                                                .m
                                                                .medium),
                                                      ),
                                                      (getSelectedBottomSheetModel(
                                                              displayItems[
                                                                  index]))
                                                          ? Icon(Icons.check,
                                                              size: 16,
                                                              color: AppLocator
                                                                  .color
                                                                  .primary)
                                                          : const SizedBox()
                                                    ],
                                                  ),
                                                  const SizedBox(height: 16)
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                        separatorBuilder: (context, index) {
                                          return Divider(
                                              height: 1,
                                              color: AppLocator
                                                  .color.grey.shade400);
                                        }),
                                  )
                                : Expanded(
                                    child: Center(
                                      child: Text(
                                        'Chưa có dữ liệu',
                                        style:
                                            AppLocator.theme.textThemeBlack.m,
                                      ),
                                    ),
                                  )
                          ],
                        ),
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.symmetric(horizontal: 16),
                    //   child: Button.primary(
                    //     buttonName: 'Chọn',
                    //     onPressed: (tempSelectedItem != null)
                    //         ? () {
                    //             FocusScope.of(context).unfocus();
                    //             widget.onChangeItem(tempSelectedItem!);
                    //             Navigator.pop(context);
                    //           }
                    //         : null,
                    //   ),
                    // ),
                    // const SizedBox(height: 24)
                  ],
                )),
          )
        ],
      ),
    );
  }
}
