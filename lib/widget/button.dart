import 'package:base_provider/common/extension/text_style.dart';
import 'package:flutter/material.dart';
import '../di/locator.shorten.dart';

class Button extends StatelessWidget {
  final VoidCallback? onPressed;
  final double padding;
  final String? buttonName;
  final Widget? child;
  final Color disableColor;
  final Color backgroundColor;

  const Button.raw({
    Key? key,
    this.buttonName,
    this.child,
    this.onPressed,
    required this.padding,
    required this.disableColor,
    required this.backgroundColor,
  }) : super(key: key);

  factory Button.primary(
          {required String buttonName, VoidCallback? onPressed}) =>
      Button.raw(
        padding: 14,
        onPressed: onPressed,
        buttonName: buttonName,
        disableColor: AppLocator.color.grey.shade600,
        backgroundColor: AppLocator.color.primary,
      );

  factory Button.square(
          {required Widget child, required VoidCallback onPressed}) =>
      Button.raw(
        padding: 8,
        onPressed: onPressed,
        disableColor: AppLocator.color.grey.shade600,
        backgroundColor: AppLocator.color.white,
        child: child,
      );

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all(Size.zero),
        padding: MaterialStateProperty.all(
            EdgeInsets.symmetric(vertical: padding, horizontal: padding)),
        shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
        backgroundColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.disabled)) {
            return disableColor;
          }
          return backgroundColor;
        }),
      ),
      onPressed: onPressed,
      child: child ?? Text(buttonName!, style: AppLocator.theme.textThemeWhite.lg.medium),
    );
  }
}
