import 'package:flutter/material.dart';
import '../di/locator.shorten.dart';

class RadioItem<T> extends StatelessWidget {
  final T groupValue;
  final ValueChanged<T> onChanged;
  final T value;
  final String text;

  const RadioItem(
      {super.key,
      required this.groupValue,
      required this.onChanged,
      required this.value,
      required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Radio(
          visualDensity: const VisualDensity(
            horizontal: VisualDensity.minimumDensity,
            vertical: VisualDensity.minimumDensity,
          ),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          value: value,
          activeColor: AppLocator.color.primary,
          onChanged: (T? value) {
            if (value != null) {
              onChanged(value);
            }
          },
          groupValue: groupValue,
        ),
        const SizedBox(width: 4,),
        Text(text, style: AppLocator.theme.textThemeBlack.m),
      ],
    );
  }
}
