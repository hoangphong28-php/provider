// import 'package:flutter/material.dart';
// import '../di/locator.dart';

// class TestComponent extends StatefulWidget {
//   const TestComponent({super.key});

//   @override
//   State<TestComponent> createState() => _TestComponentState();
// }

// class _TestComponentState extends State<TestComponent> {
//   ClaimApi claimApi = locator<ClaimApi>();

//   List<DemoModel> list = [
//     DemoModel(id: 'nam', name: 'nam'),
//     DemoModel(id: 'nu', name: 'nu'),
//     DemoModel(id: 'bede', name: 'be de'),
//     DemoModel(id: 'sieubede', name: 'sieu be de'),
//   ];

//   Future<List<DemoModel>> getInitialItem() async {
//     await Future.delayed(const Duration(seconds: 5));
//     return [
//       DemoModel(id: 'nam', name: 'nam'),
//       DemoModel(id: 'nu', name: 'nu'),
//       DemoModel(id: 'bede', name: 'be de'),
//       DemoModel(id: 'sieubede', name: 'sieu be de'),
//     ];
//   }

//   DemoModel? model;

//   AttachmentModel? image;

//   Map<String, List<AttachmentModel>> attachmentGroups = {};
//   TextEditingController control = TextEditingController();
//   TextEditingController nextControl = TextEditingController(text: '01/05/2022');

//   @override
//   void initState() {
//     super.initState();
//     attachmentGroups = {
//       "19": [
//         AttachmentModel(
//             docGroup: 4,
//             attachmentTypeId: 19,
//             maximum: 5,
//             attachmentTypeName: 'Truoc',
//             attachmentType: AttachmentType.image)
//       ],
//       // "20": [
//       //   AttachmentModel(
//       //       docGroup: 4,
//       //       attachmentTypeId: 20,
//       //       maximum: 6,
//       //       attachmentTypeName: 'Sau',
//       //       attachmentType: AttachmentType.video)
//       // ]
//     };
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(),
//       body: SafeArea(
//         child: Column(
//           children: [
//             const SizedBox(
//               height: 32,
//             ),
//             GestureDetector(
//               onTap: () {
//                 showAppDialog(
//                   // icon: SvgPicture.asset(AppIcons.home),
//                   // buttonName: 'Dong y',
//                   content:
//                       'xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h',
//                   // body: const Text(
//                   //   'xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h xin thong bao bay gio la 19h',
//                   //   textAlign: TextAlign.center,
//                   // ),
//                   // onTap: () {
//                   //   print('object hahaha');
//                   // },
//                 );
//               },
//               child: const SizedBox(
//                 height: 40,
//                 child: Text('showDialog'),
//               ),
//             ),
//             const SizedBox(height: 12),
//             SingleChoiceBottomSheet(
//               getInitialItems: getInitialItem,
//               onChangeItem: (DemoModel data) {
//                 setState(() {
//                   model = data;
//                 });
//               },
//               hint: 'abc',
//               label: 'bcd',
//               selectedItem: model,
//             ),
//             const SizedBox(
//               height: 12,
//             ),
//             Expanded(
//               child: ListView(
//                 children: [
//                   SelectMultiImage(
//                       docGroup: ClaimDocGroup.document,
//                       attachmentGroups: attachmentGroups,
//                       imageHeight: 80,
//                       imageWidth: 80,
//                       folderId: 22),
//                 ],
//               ),
//             ),
//             // ImageItem(
//             //   title: 'CCCD',
//             //   imageHeight: 80,
//             //   imageWidth: 120,
//             //   initialImage: image,
//             //   onImageChanged: (file) {
//             //     setState(() {
//             //       image = file;
//             //     });
//             //   },
//             //   attachmentType: ClaimAttachmentType(
//             //       docGroup: 2, attachmentTypeId: 19, maximum: 1),
//             //   folderId: 22,
//             // ),
//             const SizedBox(height: 12),
           
//             const SizedBox(height: 12),
//             // DateTimeInput(
//             //   controller: control,
//             //   hintText: 'hh:mm- dd/MM/yyyy',
//             // )
//             DateInput(
//               controller: control,
//               hintText: 'now',
//               onChange: (res) {
//                 setState(() {
//                   control.text = res;
//                 });
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
