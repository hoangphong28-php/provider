abstract class BaseItem {
  String get displayName;
  String get uniqueData;
}
