import 'package:flutter/material.dart';
import '../../di/locator.shorten.dart';
import 'input.dart';
import 'input_type.dart';

class LabelInput extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  final InputType inputType;
  final bool isEnable;
  final bool isRequired;

  LabelInput(
      {super.key,
      required this.label,
      required this.controller,
      inputType,
      this.isEnable = true,
      this.isRequired = true})
      : inputType = inputType ?? InputType.text();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: AppLocator.theme.textThemeGrey.m,
            ),
            (isRequired) ? Padding(

              padding: const EdgeInsets.only(left: 4),
              child: Text('*', style: AppLocator.theme.textThemeError.m,),
            ) : const SizedBox()
          ],
        ),
        const SizedBox(height: 8),
        Input.raw(
          controller: controller,
          autofocus: false,
          inputType: inputType,
          hintText: 'Nhập $label',
          isEnable: isEnable,
        )
      ],
    );
  }
}
