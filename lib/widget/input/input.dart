import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../app/res/icons.dart';
import '../../di/locator.shorten.dart';
import 'input_type.dart';

class Input extends StatefulWidget {
  Input.raw(
      {super.key,
      required this.controller,
      this.autofocus = false,
      this.obscure = false,
      required this.inputType,
      this.hintText = '',
      this.prefixIcon,
      this.suffixIcon,
      this.isEnable = true,
      this.onChanged,
      this.onEditingComplete,
        fillColor})
      : fillColor = fillColor ?? AppLocator.color.grey.shade400;

  final TextEditingController controller;
  final bool autofocus;
  final bool obscure;
  final InputType inputType;
  final String hintText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? isEnable;
  final ValueChanged<String>? onChanged;
  final VoidCallback? onEditingComplete;
  final Color? fillColor;

  factory Input.text(
          {required TextEditingController controller,
          bool autofocus = false,
          String hintText = '',
          Widget? prefixIcon,
          Widget? suffixIcon,
          ValueChanged<String>? onChanged,
          VoidCallback? onEditingComplete,
          Color? fillColor,
          bool? isEnable}) =>
      Input.raw(
        controller: controller,
        autofocus: autofocus,
        inputType: InputType.text(),
        hintText: hintText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        onChanged: onChanged,
        onEditingComplete: onEditingComplete,
        fillColor: fillColor,
        isEnable: isEnable,
      );

  factory Input.currency(
      {required TextEditingController controller,
        bool autofocus = false,
        String hintText = '',
        Widget? prefixIcon,
        Widget? suffixIcon,
        ValueChanged<String>? onChanged,
        Color? fillColor,
        bool? isEnable}) =>
      Input.raw(
        controller: controller,
        autofocus: autofocus,
        inputType: InputType.currency(),
        hintText: hintText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        onChanged: onChanged,
        fillColor: fillColor,
        isEnable: isEnable,
      );

  factory Input.number(
          {required TextEditingController controller,
          bool autofocus = false,
          String hintText = '',
          Widget? prefixIcon,
          Widget? suffixIcon,
          ValueChanged<String>? onChanged,
            Color? fillColor,
            bool? isEnable}) =>
      Input.raw(
        controller: controller,
        autofocus: autofocus,
        inputType: InputType.number(),
        hintText: hintText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        onChanged: onChanged,
        fillColor: fillColor,
        isEnable: isEnable,
      );

  factory Input.email(
          {required TextEditingController controller,
          bool autofocus = false,
          String hintText = '',
          Widget? prefixIcon,
          Widget? suffixIcon,
          ValueChanged<String>? onChanged}) =>
      Input.raw(
        controller: controller,
        autofocus: autofocus,
        inputType: InputType.email(),
        hintText: hintText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        onChanged: onChanged,
      );

  factory Input.password(
          {required TextEditingController controller,
          bool autofocus = false,
          String hintText = '',
          Widget? prefixIcon}) =>
      Input.raw(
          controller: controller,
          autofocus: autofocus,
          obscure: true,
          inputType: InputType.password(),
          hintText: hintText,
          prefixIcon: prefixIcon);

  @override
  State<StatefulWidget> createState() {
    return _InputState();
  }
}

class _InputState extends State<Input> {
  bool isEmpty = true;
  bool _isHidden = false;

  @override
  void initState() {
    super.initState();
    if (widget.obscure) {
      _isHidden = true;
    }
    isEmpty = widget.controller.text.isEmpty;
  }

  Widget buildSuffixIcon() {
    if (widget.obscure) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: InkWell(
          onTap: () {
            setState(() {
              _isHidden = !_isHidden;
            });
          },
          // child: _isHidden
          //     ? SvgPicture.asset(
          //         AppIcons.eye,
          //         color: AppLocator.color.grey,
          //       )
          //     : SvgPicture.asset(
          //         AppIcons.eyeSlash,
          //         color: AppLocator.color.grey,
          //       ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: widget.suffixIcon ??
          InkWell(
            onTap: () {
              setState(() {
                isEmpty = true;
              });
              widget.controller.clear();
              if (widget.onChanged != null) {
                widget.onChanged!(widget.controller.text);
              }
            },
            child: (isEmpty)
                ? const SizedBox()
                : Icon(
                    Icons.clear,
                    color: AppLocator.color.grey,
                  ),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder enabledInputBorder = OutlineInputBorder(
      borderSide: const BorderSide(color: Colors.transparent),
      borderRadius: BorderRadius.circular(8),
    );
    OutlineInputBorder focusedInputBorder = OutlineInputBorder(
      borderSide: BorderSide(color: AppLocator.color.primary),
      borderRadius: BorderRadius.circular(8),
    );
    OutlineInputBorder errorInputBorder = OutlineInputBorder(
      borderSide: BorderSide(color: AppLocator.color.red),
      borderRadius: BorderRadius.circular(8),
    );

    return TextFormField(
      enabled: widget.isEnable,
      controller: widget.controller,
      autofocus: widget.autofocus,
      obscureText: _isHidden,
      style: AppLocator.theme.textThemeBlack.m,
      keyboardType: widget.inputType.keyboardType,
      inputFormatters: widget.inputType.inputFormatters,
      onChanged: (value) {
        if ((value.isNotEmpty && isEmpty) || (value.isEmpty && !isEmpty)) {
          setState(() {
            isEmpty = value.isEmpty;
          });
        }
        if (widget.onChanged != null) {
          widget.onChanged!(value);
        }
      },
      onEditingComplete: widget.onEditingComplete,
      decoration: InputDecoration(
          fillColor: widget.fillColor,
          border: InputBorder.none,
          contentPadding: const EdgeInsets.symmetric(vertical: 14),
          prefixIconConstraints: const BoxConstraints(
            maxHeight: 16,
          ),
          suffixIconConstraints: const BoxConstraints(
            maxHeight: 16,
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: widget.prefixIcon,
          ),
          isDense: true,
          suffixIcon: buildSuffixIcon(),
          filled: true,
          enabledBorder: enabledInputBorder,
          disabledBorder: enabledInputBorder,
          focusedBorder: focusedInputBorder,
          errorBorder: errorInputBorder,
          focusedErrorBorder: errorInputBorder,
          hintText: widget.hintText,
          hintStyle: AppLocator.theme.textThemeGrey.m,
          errorStyle: AppLocator.theme.textThemeError.sm),
      validator: widget.inputType.validator,
    );
  }
}
