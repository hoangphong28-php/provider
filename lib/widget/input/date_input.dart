import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';


import '../../app/res/icons.dart';
import '../../common/extension/datetime.dart';
import '../../di/locator.shorten.dart';
import 'input.dart';
import 'input_type.dart';

class DateInput extends StatelessWidget {
  final String? initialDate;
  final String? maximumDate;
  final String? minimumDate;
  final CupertinoDatePickerMode? mode;
  final DateFormat? displayFormat;
  final TextEditingController controller;
  final String? labelText;
  final String? hintText;
  final bool isEnable;
  final ValueChanged<String> onChange;

  DateInput({
    super.key,
    this.initialDate,
    this.maximumDate,
    this.minimumDate,
    this.isEnable = true,
    CupertinoDatePickerMode? mode,
    DateFormat? displayFormat,
    required this.controller,
    this.labelText,
    String? hintText,
    required this.onChange,
  })  : displayFormat = displayFormat ?? dateFormatter,
        hintText = hintText ?? 'dd/mm/yyyy',
        mode = mode ?? CupertinoDatePickerMode.date;

  void showCustomCupertinoModalPopup<T>(
      {required BuildContext context,
      required Widget body,
      required VoidCallback onDone,
      String? title,
      T? initialValue}) {
    Size screenSize = MediaQuery.of(context).size;

    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Material(
              color: Colors.transparent,
              textStyle: AppLocator.theme.textThemeBlack.m,
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                /// Title bar
                Container(
                    height: 52,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(13),
                          topRight: Radius.circular(13)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          splashRadius: 20,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: const Icon(Icons.close),
                        ),
                        Expanded(
                            child: Text(
                          title ?? '',
                          textAlign: TextAlign.center,
                          style: AppLocator.theme.textThemeBlack.m,
                        )),
                        CupertinoButton(
                          // ignore: sort_child_properties_last
                          child: Text(
                            'Xong',
                            style: AppLocator.theme.textThemeBlack.m,
                          ),
                          onPressed: () {
                            onDone();
                            // Modular.to.pop();
                            Navigator.pop(context);
                          },
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16.0,
                            vertical: 5.0,
                          ),
                        )
                      ],
                    )),
                Divider(height: 1, color: AppLocator.color.grey.shade200),

                /// List
                Container(
                  height: screenSize.height / 2.5,
                  color: const Color(0xFFFFFFFF),
                  child: body,
                )
              ]));
        });
  }

  void showInputDatePicker(
      {required BuildContext context,
      DateTime? initialDate,
      DateTime? maximumDate,
      DateTime? minimumDate,
      String? title,
      required ValueChanged<DateTime?> onTap,
      CupertinoDatePickerMode mode = CupertinoDatePickerMode.date}) {
    DateTime selectedDate = initialDate ?? DateTime.now();
    showCustomCupertinoModalPopup(
        context: context,
        title: title,
        onDone: () {
          onTap(selectedDate);
        },
        body: CupertinoDatePicker(
          initialDateTime: selectedDate,
          mode: mode,
          use24hFormat: true,
          dateOrder: DatePickerDateOrder.dmy,
          onDateTimeChanged: (DateTime date) {
            selectedDate = date;
          },
          maximumDate: maximumDate,
          minimumDate: minimumDate,
        ));
  }

  DateTime? getMaximumDate() {
    DateTime? date;
    if (maximumDate != null && maximumDate != '') {
      if (controller.text.isNotEmpty) {
        if (displayFormat!
            .parse(maximumDate!)
            .isBefore(displayFormat!.parse(controller.text))) {
          date = displayFormat!.parse(controller.text);
        } else {
          date = displayFormat!.parse(maximumDate!);
        }
      } else {
        date = displayFormat!.parse(maximumDate!);
      }
    }
    return date;
  }

  DateTime? getMinimumDate() {
    DateTime? date;
    if (minimumDate != null && minimumDate != '') {
      if (controller.text.isNotEmpty) {
        if (displayFormat!
            .parse(minimumDate!)
            .isAfter(displayFormat!.parse(controller.text))) {
          date = displayFormat!.parse(controller.text);
        } else {
          date = displayFormat!.parse(minimumDate!);
        }
      } else {
        date = displayFormat!.parse(minimumDate!);
      }
    }
    return date;
  }

  DateTime? getInitialDate() {
    DateTime? date;

    if (controller.text.isNotEmpty) {
      if (minimumDate != null && minimumDate != '') {
        if (displayFormat!
            .parse(controller.text)
            .isAfter(displayFormat!.parse(minimumDate!))) {
          date = displayFormat!.parse(controller.text);
        } else if (displayFormat!
            .parse(controller.text)
            .isBefore(displayFormat!.parse(minimumDate!))) {
          date = displayFormat!.parse(minimumDate!);
        }
      } else if (maximumDate != null && maximumDate != '') {
        if (displayFormat!
            .parse(controller.text)
            .isBefore(displayFormat!.parse(maximumDate!))) {
          date = displayFormat!.parse(controller.text);
        } else if (displayFormat!
            .parse(controller.text)
            .isAfter(displayFormat!.parse(maximumDate!))) {
          date = displayFormat!.parse(maximumDate!);
        }
      } else {
        date = displayFormat!.parse(controller.text);
      }
    } else {
      if (initialDate != null && initialDate != '') {
        if (minimumDate != null && minimumDate != '') {
          if (displayFormat!
              .parse(initialDate!)
              .isAfter(displayFormat!.parse(minimumDate!))) {
            date = displayFormat!.parse(initialDate!);
          } else if (displayFormat!
              .parse(initialDate!)
              .isBefore(displayFormat!.parse(minimumDate!))) {
            date = displayFormat!.parse(minimumDate!);
          }
        } else if (maximumDate != null && maximumDate != '') {
          if (displayFormat!
              .parse(initialDate!)
              .isBefore(displayFormat!.parse(maximumDate!))) {
            date = displayFormat!.parse(initialDate!);
          } else if (displayFormat!
              .parse(initialDate!)
              .isAfter(displayFormat!.parse(maximumDate!))) {
            date = displayFormat!.parse(maximumDate!);
          }
        }
      } else {
        if (minimumDate != null && minimumDate != '') {
          if (DateTime.now().isAfter(displayFormat!.parse(minimumDate!))) {
            date = DateTime.now();
          } else if (DateTime.now()
              .isBefore(displayFormat!.parse(minimumDate!))) {
            date = displayFormat!.parse(minimumDate!);
          }
        } else if (maximumDate != null && maximumDate != '') {
          if (DateTime.now().isBefore(displayFormat!.parse(maximumDate!))) {
            date = DateTime.now();
          } else if (DateTime.now()
              .isAfter(displayFormat!.parse(maximumDate!))) {
            date = displayFormat!.parse(maximumDate!);
          }
        }
      }
    }
    return date;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            if (isEnable) {
              showInputDatePicker(
                  context: context,
                  initialDate: getInitialDate(),
                  maximumDate: getMaximumDate(),
                  minimumDate: getMinimumDate(),
                  mode: mode!,
                  title: labelText,
                  onTap: (DateTime? selectedDate) {
                    if (selectedDate != null) {
                      // controller.text = displayFormat!.format(selectedDate);
                      onChange(displayFormat!.format(selectedDate));
                    }
                  });
            }
          },
          child: Input.raw(
            controller: controller,
            inputType: InputType.text(),
            isEnable: false,
            // suffixIcon: SvgPicture.asset(AppIcons.calendar),
            hintText: hintText!,
          )),
    );
  }
}
