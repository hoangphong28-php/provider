import 'package:base_provider/widget/input/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../app/res/icons.dart';
import '../../common/extension/datetime.dart';
import '../../di/locator.shorten.dart';
import 'input_type.dart';

class DateTimeInput extends StatelessWidget {
  final TextEditingController controller;
  final String? hintText;
  final String? initialDate;
  final DateTime? maximumDate;
  final String? minimumDate;
  final bool isEnable;

  const DateTimeInput(
      {super.key,
      required this.controller,
      this.isEnable = true,
      this.hintText,
      this.initialDate,
      this.maximumDate,
      this.minimumDate});

  void showCustomCupertinoModalPopup<T>(
      {required BuildContext context,
      required Widget body,
      required VoidCallback onDone,
      String? title,
      T? initialValue}) {
    Size screenSize = MediaQuery.of(context).size;

    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Material(
              color: Colors.transparent,
              textStyle: AppLocator.theme.textThemeBlack.m,
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                /// Title bar
                Container(
                    height: 52,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(13),
                          topRight: Radius.circular(13)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          splashRadius: 20,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: const Icon(Icons.close),
                        ),
                        Expanded(
                            child: Text(
                          title ?? '',
                          textAlign: TextAlign.center,
                          style: AppLocator.theme.textThemeBlack.m,
                        )),
                        CupertinoButton(
                          // ignore: sort_child_properties_last
                          child: Text(
                            'Xong',
                            style: AppLocator.theme.textThemeBlack.m,
                          ),
                          onPressed: () {
                            onDone();
                            // Modular.to.pop();
                            Navigator.pop(context);
                          },
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16.0,
                            vertical: 5.0,
                          ),
                        )
                      ],
                    )),
                Divider(height: 1, color: AppLocator.color.grey.shade200),

                /// List
                Container(
                  height: screenSize.height / 2.5,
                  color: const Color(0xFFFFFFFF),
                  child: body,
                )
              ]));
        });
  }

  void showInputDatePicker(
      {required BuildContext context,
      DateTime? initialDate,
      DateTime? maximumDate,
      DateTime? minimumDate,
      String? title,
      required ValueChanged<DateTime?> onTap}) {
    DateTime selectedDate = initialDate ?? DateTime.now();
    showCustomCupertinoModalPopup(
      context: context,
      title: title,
      onDone: () {
        onTap(selectedDate);
      },
      body: SizedBox(
          child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Flexible(
            flex: 7,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              initialDateTime: initialDate,
              maximumDate: maximumDate,
              minimumDate: minimumDate,
              onDateTimeChanged: (DateTime dateTime) {
                selectedDate = DateTime(dateTime.year, dateTime.month,
                    dateTime.day, selectedDate.hour, selectedDate.minute);
              },
            ),
          ),
          Flexible(
            flex: 4,
            child: CupertinoDatePicker(
              use24hFormat: true,
              mode: CupertinoDatePickerMode.time,
              initialDateTime: initialDate,
              maximumDate: maximumDate,
              minimumDate: minimumDate,
              onDateTimeChanged: (DateTime dateTime) {
                selectedDate = DateTime(selectedDate.year, selectedDate.month,
                    selectedDate.day, dateTime.hour, dateTime.minute);
              },
            ),
          ),
        ],
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (isEnable) {
          showInputDatePicker(
            initialDate: (initialDate != null)
                ? dateFormatter.parse(initialDate!)
                : ((controller.text.isNotEmpty)
                ? dateTimeFormatter.parse(controller.text)
                : null),
            minimumDate: (minimumDate != null)
                ? dateTimeFormatter.parse(minimumDate!)
                : null,
            maximumDate: maximumDate,
            context: context,
            onTap: (DateTime? res) {
              if (res != null) {
                controller.text = dateTimeFormatter.format(res);
              }
            },
          );
        }

      },
      child: SizedBox(
          child: Input.raw(
        controller: controller,
        inputType: InputType.text(),
        isEnable: false,
        // suffixIcon: SvgPicture.asset(AppIcons.calendar),
        hintText: hintText!,
      )),
    );
  }
}
