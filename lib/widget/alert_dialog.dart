import 'package:base_provider/common/extension/extension.dart';
import 'package:flutter/material.dart';
import '../di/locator.shorten.dart';
import 'button.dart';

class DialogBody extends StatelessWidget {
  final Widget? icon;
  final Widget? body;
  final String? title;
  final String? content;
  final String? buttonName;
  final VoidCallback? onTap;

  const DialogBody({
    super.key,
    this.icon,
    this.title,
    this.body,
    this.content,
    this.buttonName,
    this.onTap,
  });
  // : assert(
  //       (body != null)
  //           ? (icon == null &&
  //               content == null &&
  //               buttonName == null &&
  //               onTap == null)
  //           : (body == null &&
  //               (icon != null &&
  //                   content != null &&
  //                   buttonName != null &&
  //                   onTap != null)),
  //       'Sai roi');

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 32),
      decoration: BoxDecoration(
          color: AppLocator.color.white,
          borderRadius: BorderRadius.circular(16)),
      child: body ??
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              icon != null
                  ? SizedBox(height: 60, width: 60, child: icon)
                  : const SizedBox(),
              icon != null ? const SizedBox(height: 24) : const SizedBox(),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    title ?? 'Thông báo',
                    textAlign: TextAlign.center,
                    style: AppLocator.theme.textThemeBlack.xl.medium,
                  ),
                  const SizedBox(height: 14),
                  Text(
                    content ?? '',
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 24),
                  Button.primary(
                    buttonName: buttonName ?? 'OK',
                    onPressed: onTap ??
                        () {
                          AppLocator.to.pop();
                        },
                  )
                ],
              )
            ],
          ),
    );
  }
}

void showAppDialog({
  barrierDismissible = false,
  Widget? icon,
  Widget? body,
  String? content,
  String? buttonName,
  String? titleName,
  VoidCallback? onTap,
  bool showCloseButton = true,
}) {
  showDialog(
    barrierDismissible: barrierDismissible,
    context: AppLocator.context,
    builder: (context) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Dialog(
          backgroundColor: Colors.transparent,
          insetPadding: const EdgeInsets.all(16),
          child: Stack(
            children: [
              DialogBody(
                icon: icon,
                body: body,
                title: titleName,
                buttonName: buttonName,
                content: content,
                onTap: onTap,
              ),
              (showCloseButton)
                  ? Positioned(
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          AppLocator.to.pop();
                        },
                        child: Container(
                          height: 32,
                          width: 32,
                          decoration: BoxDecoration(
                            color: AppLocator.color.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: AppLocator.color.black.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: const Center(
                            child: Icon(Icons.close, size: 16),
                          ),
                        ),
                      ),
                    )
                  : const SizedBox()
            ],
          ),
        ),
      );
    },
  );
}
