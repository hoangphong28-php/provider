import 'package:base_provider/di/locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import '../app/res/res.dart';
import '../app/route/navigator_service.dart';

class AppLocator {
  static NavigationService get to => locator<NavigationService>();

  static GlobalKey<NavigatorState> get navigationKey =>
      locator<NavigationService>().navigatorKey;

  static BuildContext get context => locator<NavigationService>().context;

  static String? get route => locator<NavigationService>().currentRoute;

  static AppThemeData get theme =>
      AppTheme.of(locator<NavigationService>().context);

  static AppColorScheme get color =>
      AppTheme.of(locator<NavigationService>().context).colorScheme;


}
