import 'package:flutter/material.dart';

class AppColorScheme {
  final Color primary;
  final Color secondary;
  final Color background;
  final MaterialColor grey;
  final Color red;
  final Color white;
  final Color black;
  final Color orange;
  final Color blue;
  final Color green;

  AppColorScheme(
      {required this.primary,
      required this.orange,
      required this.green,
      required this.blue,
      required this.secondary,
      required this.grey,
      required this.background,
      required this.red,
      required this.white,
      required this.black});

  factory AppColorScheme.light() => AppColorScheme(
      primary: const Color(0xFF243468),
      blue: const Color(0xff0062FF),
      green: const Color(0xff07A201),
      secondary: const Color(0xFFA2EFEE),
      orange: const Color(0xffFF8900),
      grey: const MaterialColor(
        0xFF707070,
        {
          200: Color(0xFFF3F4F7),
          400: Color(0x0F243468),
          600: Color(0xFFAAAAAA),
        },
      ),
      background: const Color(0xFFF7FBFF),
      red: const Color(0xFFEB0000),
      white: Colors.white,
      black: Colors.black);

  factory AppColorScheme.dark() => AppColorScheme(
      primary: const Color(0xFF243468),
      blue: const Color(0xff0062FF),
      green: const Color(0xff07A201),

      secondary: const Color(0xFFA2EFEE),
      orange: const Color(0xffFF8900),
      grey: const MaterialColor(
        0xFF707070,
        {
          200: Color(0xFFF3F4F7),
          400: Color(0x0F243468),
          600: Color(0xFFAAAAAA),
        },
      ),
      background: const Color(0xFFF7FBFF),
      red: const Color(0xFFEB0000),
      white: Colors.white,
      black: Colors.black);

  AppColorScheme copyWith(
      {Color? primary,
      Color? secondary,
      Color? background,
      MaterialColor? grey,
      Color? red,
      Color? white,
      Color? black,
      Color? blue,
      Color? green,
      Color? orange}) {
    return AppColorScheme(
        blue: blue ?? this.blue,
        green: green?? this.green,
        orange: orange ?? this.orange,
        primary: primary ?? this.primary,
        secondary: secondary ?? this.secondary,
        background: background ?? this.background,
        red: red ?? this.red,
        white: white ?? this.white,
        grey: grey ?? this.grey,
        black: black ?? this.black);
  }
}
