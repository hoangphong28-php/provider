import 'package:flutter/material.dart';

class AppTextTheme {
  final TextStyle xsm;
  final TextStyle sm;
  final TextStyle m;
  final TextStyle lg;
  final TextStyle xl;
  final TextStyle xxl;

  const AppTextTheme({
    required this.xsm,
    required this.sm,
    required this.m,
    required this.lg,
    required this.xl,
    required this.xxl,
  });

  factory AppTextTheme.create(Color color) {
    return AppTextTheme(
      xsm: TextStyle(
        fontSize: 10,
        color: color,
      ),
      sm: TextStyle(
        fontSize: 12,
        color: color,
      ),
      m: TextStyle(
        fontSize: 14,
        color: color,
      ),
      lg: TextStyle(
        fontSize: 16,
        color: color,
      ),
      xl: TextStyle(
        fontSize: 20,
        color: color,
      ),
      xxl: TextStyle(
        fontSize: 28,
        color: color,
      ),
    );
  }
}