import 'package:base_provider/app/res/text_themes.dart';
import 'package:flutter/material.dart';
import 'colors.dart';

class AppThemeData {
  final Brightness brightness;
  final AppColorScheme colorScheme;
  final AppTextTheme textThemePrimary;
  final AppTextTheme textThemeSecondary;
  final AppTextTheme textThemeError;
  final AppTextTheme textThemeWhite;
  final AppTextTheme textThemeBlack;
  final AppTextTheme textThemeGreen;
  final AppTextTheme textThemeGrey;
  final AppTextTheme textThemeLGrey;

  const AppThemeData.raw({
    required this.brightness,
    required this.colorScheme,
    required this.textThemePrimary,
    required this.textThemeSecondary,
    required this.textThemeError,
    required this.textThemeWhite,
    required this.textThemeBlack,
    required this.textThemeGreen,
    required this.textThemeGrey,
    required this.textThemeLGrey,
  });

  factory AppThemeData({
    required Brightness brightness,
    required AppColorScheme colorScheme,
  }) =>
      AppThemeData.raw(
        brightness: brightness,
        colorScheme: colorScheme,
        textThemePrimary: AppTextTheme.create(colorScheme.primary),
        textThemeSecondary: AppTextTheme.create(colorScheme.secondary),
        textThemeError: AppTextTheme.create(colorScheme.red),
        textThemeWhite: AppTextTheme.create(colorScheme.white),
        textThemeBlack: AppTextTheme.create(colorScheme.black),
        textThemeGrey: AppTextTheme.create(colorScheme.grey),
        textThemeGreen: AppTextTheme.create(colorScheme.green),
        textThemeLGrey: AppTextTheme.create(colorScheme.grey.shade600),
      );

  factory AppThemeData.light() => AppThemeData(
        brightness: Brightness.light,
        colorScheme: AppColorScheme.light(),
      );

  factory AppThemeData.dark() => AppThemeData(
        brightness: Brightness.dark,
        colorScheme: AppColorScheme.dark(),
      );
}

class AppTheme extends InheritedWidget {
  final AppThemeData data;

  const AppTheme({
    Key? key,
    required this.data,
    required Widget child,
  }) : super(key: key, child: child);

  static AppThemeData of(BuildContext context) {
    final AppTheme? result =
        context.dependOnInheritedWidgetOfExactType<AppTheme>();
    assert(result != null, 'No AppTheme found in context');
    return result!.data;
  }

  @override
  bool updateShouldNotify(AppTheme oldWidget) => data != oldWidget.data;
}
