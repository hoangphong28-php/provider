import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../../di/locator.shorten.dart';

@lazySingleton
class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Map inAppRouting = {};

  Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState!.pushNamed(routeName);
  }

  Future<T?> push<T extends Object?>(Widget screen) {
    return navigatorKey.currentState!
        .push(MaterialPageRoute(builder: (_) => screen));
  }

  Future<T?> pushReplacement<T extends Object?>(Widget screen) {
    return navigatorKey.currentState!
        .pushReplacement(MaterialPageRoute(builder: (_) => screen));
  }

  Future<T?> pushNamed<T extends Object?>(String routeName,
      {Object? arguments}) {
    return navigatorKey.currentState!
        .pushNamed(routeName, arguments: arguments);
  }

  Future<T?> pushReplacementNamed<T extends Object?>(String routeName,
      {Object? arguments}) {
    return navigatorKey.currentState!
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  Future<T?> pushNamedAndRemoveUntil<T extends Object?>(String routeName,
      {Object? arguments, required RoutePredicate predicate}) {
    return navigatorKey.currentState!
        .pushNamedAndRemoveUntil(routeName, predicate, arguments: arguments);
  }

  void pop<T extends Object?>({
    T? result,
    bool? untilRoot,
    String? untilRoute,
  }) {
    final navigator = navigatorKey.currentState!;
    if (!navigator.canPop()) return;
    if (untilRoot == true) {
      navigator.popUntil((route) => route.isFirst);
    } else if (untilRoute != null) {
      navigator.popUntil((route) => route.settings.name == untilRoute);
    } else {
      navigator.pop(result);
    }
  }

  String get currentRoute => _getCurrentRoute();

  String _getCurrentRoute() {
    late String currentRoute;
    final navigator = navigatorKey.currentState!;
    navigator.popUntil((route) {
      if (route.isCurrent) {
        currentRoute = route.settings.name!;
      }
      return true;
    });
    return currentRoute;
  }

  BuildContext get context => navigatorKey.currentContext!;

  void redirectAfterLogin() {
    if (inAppRouting.isNotEmpty) {
      pushNamed('${inAppRouting['routeName']}', arguments: jsonDecode(inAppRouting['routeArgument']));
    }
    inAppRouting = {};
  }

  // void notificationRedirect(Map<dynamic, dynamic> payload){
  //   if (AppLocator.user.isLoggedIn()) {
  //     pushNamed('${payload['routeName']}', arguments: jsonDecode(payload['routeArgument']));
  //     inAppRouting = {};
  //   } else {
  //     inAppRouting = payload;
  //   }
  // }

}
