
import 'package:base_provider/module/second_page/second_page.dart';
import 'package:base_provider/module/third_page/third_page.dart';
import 'package:flutter/material.dart';

import '../../module/my_home_page/my_home_page.dart';

class AppRoute {
  static const root = '/';
  static const second = '/second';
  static const third = '/3rd';

  static Route<dynamic> getRoutes(RouteSettings settings) {
    final dynamic args = settings.arguments;
    switch (settings.name) {
      case AppRoute.root:
        return MaterialPageRoute(
            settings: const RouteSettings(name: AppRoute.root),
            builder: (context) => const MyHomePage());
      case AppRoute.second:
        return MaterialPageRoute(
            settings: const RouteSettings(name: AppRoute.second),
            builder: (context) => const SecondPage());
      case AppRoute.third:
        return MaterialPageRoute(
            settings: const RouteSettings(name: AppRoute.third),
            builder: (context) => const ThirdPage());
      default:
        throw Exception("Route ${settings.name} is not defined");
    }
  }
}
