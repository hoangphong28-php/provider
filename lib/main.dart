import 'package:base_provider/module/my_home_page/my_home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'app/res/res.dart';
import 'app/route/app_route.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/route/navigator_service.dart';
import 'di/locator.dart';

// const bool isProduction = bool.fromEnvironment('dart.vm.product');
late SharedPreferences prefs;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  // await clearAllStorage();
  // await Hive.initFlutter();
  configureDependencies();
  runApp(const MyApp());
}

Future<void> clearAllStorage() async {
  FlutterSecureStorage storage = const FlutterSecureStorage();
  try {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('first_run') ?? true) {
      await storage.deleteAll();
      prefs.setBool('first_run', false);
    }
  } catch (ex) {
    debugPrint(ex.toString());
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return AppTheme(
        data: AppThemeData.light(),
        child: MultiProvider(
          providers: [
            ChangeNotifierProvider<MyHomeViewModel>(
              create: (context) => MyHomeViewModel(),
            ),
          ],
          child: MaterialApp(
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: const [Locale('vi', 'VN')],
            navigatorKey: locator<NavigationService>().navigatorKey,
            debugShowCheckedModeBanner: false,
            title: 'provider',
            initialRoute: AppRoute.root,
            onGenerateRoute: AppRoute.getRoutes,
            builder: (context, child) {
              final mediaQueryData = MediaQuery.of(context);
              final scale = mediaQueryData.textScaleFactor.clamp(1.0, 1.1);
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
                child: GestureDetector(
                  onTap: () {
                    print('object hahahahahaha');
                  },
                  child: child!,
                ),
              );
            },
          ),
        ));
  }
}
