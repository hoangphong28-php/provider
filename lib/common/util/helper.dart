bool isEmpty(var object) {
  if (object == false ||
      object == 'false' ||
      object == 'null' ||
      object == 'N/A' ||
      object == null ||
      object == {} ||
      object == '') {
    return true;
  }
  if (object is Iterable && object.isEmpty) {
    return true;
  }
  return false;
}

bool validateNameOfPerson(var object) {
  RegExp regExpName = RegExp(
    r"^(([A-Za-zÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+[,.]?[ ]?|[a-z]+['-]?)+)$",
    caseSensitive: false,
    multiLine: false,
  );
  if (regExpName.hasMatch(object)) {
    return true;
  }
  return false;
}

bool validatorEmail(var object) {
  RegExp regex = RegExp(r'\w+@\w+\.\w+');
  if (regex.hasMatch(object)) {
    return true;
  }
  return false;
}

bool validatorCardNumber(var object) {
  if (object.isEmpty) {
    return false;
  } else if (object.length < 9) {
    return false;
  } else if (object.length > 12) {
    return false;
  } else if (object.length == 10) {
    return false;
  } else if (object.length == 11) {
    return false;
  }
  return true;
}

String formatPhoneNumber(String phoneNumber) {
  return phoneNumber.indexOf('0') == 0
      ? '84${phoneNumber.substring(1)}'
      : phoneNumber;
}
