extension ListExt on List {
  bool get notContainNull {
    bool result = true;
    for (var i in this) {
      if (i == null) {
        result = false;
      }
    }
    return result;
  }

  swapIndex(int swapIndex) {
    if (swapIndex != -1) {
      var item = this[swapIndex];
      removeAt(swapIndex);
      insert(0, item);
    }
  }

  removeNull() {
    removeWhere((element) => element == null);
  }
}
