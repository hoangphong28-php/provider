

import 'package:intl/intl.dart';

final dateFormatter = DateFormat('dd/MM/yyyy');
final timeFormatter = DateFormat('HH:mm');
final dateTimeFormatter = DateFormat('HH:mm:ss - dd/MM/yyyy');